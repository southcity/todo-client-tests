// @flow

import * as React from 'react';
import { TaskList, TaskDetails, TaskNew, TaskEdit } from '../src/task-components';
import { type Task } from '../src/task-service';
import { shallow } from 'enzyme';
import { Form, Button, Row, Column } from '../src/widgets';
import { NavLink } from 'react-router-dom';

jest.mock('../src/task-service', () => {
  class TaskService {
    getAll() {
      return Promise.resolve([
        {
          id: 1,
          title: 'Les leksjon',
          description: 'Les dagens leksjon og få et innblikk i hvordan man skriver tester i React',
          done: false,
        },
        {
          id: 2,
          title: 'Møt opp på forelesning',
          description: 'Møt opp på forelesningen for å få spørsmål besvart',
          done: false,
        },
        {
          id: 3,
          title: 'Gjør øving',
          description: 'Fullfør øvingen og få den godkjent av studass',
          done: false,
        },
      ]);
    }

    get(id: number) {
      return Promise.resolve({
        id: 1,
        title: 'Les leksjon',
        description: 'Les dagens leksjon og få et innblikk i hvordan man skriver tester i React',
        done: false,
      });
    }

    create(title: string) {
      return Promise.resolve(4); // Same as: return new Promise((resolve) => resolve(4));
    }

    update(id: number, title: string, description: string, done: string) {
      return Promise.resolve();
    }

    delete(id: number) {
      return Promise.resolve();
    }
  }
  return new TaskService();
});

describe('Task component tests', () => {
  test('TaskList draws correctly', (done) => {
    const wrapper = shallow(<TaskList />);

    // Wait for events to complete
    setTimeout(() => {
      expect(
        wrapper.containsAllMatchingElements([
          <NavLink to="/tasks/1">Les leksjon</NavLink>,
          <NavLink to="/tasks/2">Møt opp på forelesning</NavLink>,
          <NavLink to="/tasks/3">Gjør øving</NavLink>,
        ])
      ).toEqual(true);
      done();
    });
  });

  test('TaskNew correctly sets location on create', (done) => {
    const wrapper = shallow(<TaskNew />);

    wrapper.find(Form.Input).simulate('change', { currentTarget: { value: 'Kaffepause' } });
    // $FlowExpectedError
    expect(wrapper.containsMatchingElement(<Form.Input value="Kaffepause" />)).toEqual(true);

    wrapper.find(Button.Success).simulate('click');
    // Wait for events to complete
    setTimeout(() => {
      expect(location.hash).toEqual('#/tasks/4');
      done();
    });
  });

  test('TaskDetails is rendered correctly', (done) => {
    const wrapper = shallow(<TaskDetails match={{ params: { id: 1 } }} />);

    setTimeout(() => {
      expect(
        wrapper.containsAllMatchingElements([
          <Column>Title:</Column>,
          <Column>Les leksjon</Column>,
          <Column>Description:</Column>,
          <Column>
            Les dagens leksjon og få et innblikk i hvordan man skriver tester i React
          </Column>,
        ])
      ).toEqual(true);

      done();
    });
  });

  test('TaskDetails matches snapshot', (done) => {
    const wrapper = shallow(<TaskDetails match={{ params: { id: 1 } }} />);

    setTimeout(() => {
      expect(wrapper).toMatchSnapshot();
      done();
    });
  });

  test('TaskEdit edits task successfully', (done) => {
    const wrapper = shallow(<TaskEdit match={{ params: { id: 1 } }} />);

    wrapper.find(Form.Input).simulate('change', { currentTarget: { value: 'Ny Oppgave' } });
    wrapper.find(Form.Textarea).simulate('change', { currentTarget: { value: 'Oppgaven...' } });

    expect(
      wrapper.containsAllMatchingElements([
        // $FlowExpectedError
        <Form.Input value="Ny Oppgave" />,
        // $FlowExpectedError
        <Form.Textarea value="Oppgaven..." />,
        // $FlowExpectedError
        <Form.Checkbox checked={false} />,
      ])
    ).toEqual(true);

    wrapper.find(Button.Success).simulate('click');

    setTimeout(() => {
      expect(location.hash).toEqual('#/tasks/1');
      done();
    });
  });

  test('TaskEdit deletes task successfully', (done) => {
    const wrapper = shallow(<TaskEdit match={{ params: { id: 1 } }} />);

    wrapper.find(Button.Danger).simulate('click');

    setTimeout(() => {
      expect(location.hash).toEqual('#/tasks');
      done();
    });
  });
});
